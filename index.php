<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="utf-8">
    <title>IGC Data Server</title>
    <meta name="description"
          content="Hochladen, Auslesen und Löschen von IGC Daten.">
    <meta name="keywords" content="IGC, API">
    <meta name="author" content="Daniel Kettemann">

    <meta name="viewport" content="width=1024">
    <link rel="stylesheet" media="screen" href="assets/css/theme.css"/>
    <link rel="stylesheet" media="screen" href="src/selfhtmlTable/tableStyles.css"/>
    <link rel="stylesheet" href="./lib/css/bootstrap4.min.css">

    <script src="./lib/js/jquery.min.js"></script>
    <script src="./lib/js/popper.min.js"></script>
    <script src="./lib/js/bootstrap4.min.js"></script>

</head>

<body>
<?php include "./src/main-content/main-content.php" ?>
</body>

</html>
