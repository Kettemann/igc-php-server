var tableContent = [
    ['GET', 'Alle IGC Dokumente auslesen', '/api/read.php', "/assets/img/screenshots/api_read.png"],
    ['POST', 'IGC Datei hochladen', '/api/upload.php', "/assets/img/screenshots/api_upload.png"],
    ['POST', 'IGC Dokument löschen', '/api/delete.php', "/assets/img/screenshots/api_delete.png"],
];

var table = document.getElementById('table');
var captionText = "geplante Endpunkte";
var link = '<a href="######" \n' +
    '   title="follow link">\n' +
    '    ###### \n' +
    '</a>';

function addCaption () {
    var caption = table.createCaption();
    caption.innerHTML = captionText;
}

function addRow (values) {
    var nextRow = table.insertRow(table.rows.length);
    for (var i = 0; i < 4; i++) {
        var cell = nextRow.insertCell();

        if (i < 2) {
            // insert the first two values as plain text
            var Inhalt = values[i];
            cell.innerHTML += Inhalt;
        } else {
            // create links for the last two items
            var innerHTML = link.replace(/######/g, values[i]);
            cell.innerHTML = innerHTML;
        }
    }
}

addCaption();
for(var i = 0; i < tableContent.length; i++){
    addRow(tableContent[i]);
}