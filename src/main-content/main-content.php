<link rel="stylesheet" href="./src/main-content/main-content.css">

<div class="main-content-component">
<?php include "./src/header/header.html" ?>

    <div class="content-element">
        <?php include "./src/selfhtmlTable/selfhtmlTable.html" ?>
    </div>

</div>
